import australiaFlag from "./australia.svg";
import britishFlag from "./british.svg";
import canadaFlag from "./canada.svg";
import chinaFlag from "./china.svg";
import eurFlag from "./eur.svg";
import indiaFlag from "./india.svg";
import japanFlag from "./japan.svg";
import uaeFlag from "./uae.svg";
import usaFlag from "./usa.svg";

export {
  australiaFlag,
  britishFlag,
  canadaFlag,
  chinaFlag,
  eurFlag,
  indiaFlag,
  japanFlag,
  uaeFlag,
  usaFlag
};
