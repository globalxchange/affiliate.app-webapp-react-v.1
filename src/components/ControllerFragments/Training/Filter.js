import React from 'react';
import { Menu, Button, Icon, Dropdown } from 'antd';

const Filter = ({ categories, active, setActive, title, isMobile }) => {
  let menu = null;

  const handleMenuClick = e => {
    setActive(e.key);
    console.log('click left button', e);
  };

  if (isMobile) {
    menu = (
      <Menu onClick={handleMenuClick}>
        {categories.map((item, index) => (
          <Menu.Item key={item.title}>
            {item.title} ({item.count})
          </Menu.Item>
        ))}
      </Menu>
    );
  }

  return (
    <>
      {isMobile ? (
        <div className="filter-wrapper">
          <Dropdown overlay={menu} trigger={['click']}>
            <Button>
              {active} <Icon type="down" />
            </Button>
          </Dropdown>
        </div>
      ) : (
        <div className="col-md-2">
          <div className="filter-wrapper">
            <h5 className="filter-title">{title || 'Filter By'}</h5>
            <ul className="filter-menu">
              {categories.map((item, index) => (
                <li
                  key={index}
                  className={`filter-menu-item${
                    active === item.title ? ' active' : ''
                  }`}
                  onClick={() => setActive(item)}
                >
                  {item.title} ({item.count})
                </li>
              ))}
            </ul>
          </div>
        </div>
      )}
    </>
  );
};

export default Filter;
