import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { GX_API_ENDPOINT } from '../../configs';
import LoadingAnimation from '../LoadingAnimation';
import DownloadLinkViewer from './DownloadLinkViewer';

const AppLinkViewer = ({ selectedPlatform }) => {
  const [appLink, setAppLink] = useState();
  const [showLink, setShowLink] = useState(false);

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
      params: { app_code: 'broker_app' },
    })
      .then(({ data }) => {
        console.log('App Logs', data);

        const app = data.logs[0] || null;

        setAppLink(app);
      })
      .catch(error => {});
  }, []);

  const onLinkOpen = () => {
    const link =
      selectedPlatform === 'Android'
        ? appLink.android_app_link || ''
        : appLink.ios_app_link || '';
    const win = window.open(link, '_blank');
    if (win != null) {
      win.focus();
    }
  };

  if (!appLink) {
    return (
      <div className="installer-container">
        <LoadingAnimation dark />
      </div>
    );
  }

  if (showLink) {
    return (
      <DownloadLinkViewer
        link={
          selectedPlatform === 'Android'
            ? appLink.android_app_link || ''
            : appLink.ios_app_link || ''
        }
      />
    );
  }

  return (
    <div className="installer-container">
      <div className="title">Download App On This Phone</div>
      <div onClick={() => setShowLink(true)} className="selection-item">
        <div className="item-label">Copy Download Link</div>
      </div>
      <div onClick={onLinkOpen} className="selection-item">
        <div className="item-label">Open Download Link</div>
      </div>
    </div>
  );
};

export default AppLinkViewer;
