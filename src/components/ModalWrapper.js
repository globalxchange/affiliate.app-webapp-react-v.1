import React from 'react';
import Modal from 'react-modal';

Modal.setAppElement('#root');

const ModalWrapper = ({ isOpen, setIsOpen, children }) => {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={() => setIsOpen(false)}
      style={modelStyle}
    >
      {children}
    </Modal>
  );
};

export default ModalWrapper;

const modelStyle = {
  overlay: {
    zIndex: '9999999999',
    backgroundColor: 'rgba(103, 103, 103, 0.75)',
  },
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    transform: 'translate(-50%, -50%)',
    border: 'none',
    background: 'none',
    overflow: 'auto',
    borderRadius: 'none',
    height: 'auto',
    display: 'flex',
    justifyContent: 'center',
    maxWidth: '80%',
    paddingTop: '5rem',
    paddingBottom: '5rem',
    paddingLeft: '0',
    paddingRight: '0',
  },
};
