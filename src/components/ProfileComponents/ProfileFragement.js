import React from 'react';
import FollowedBrands from './FollowedBrands';

const ProfileFragement = ({ activeTab, userData, followings }) => {
  let fragment;

  switch (activeTab?.title) {
    case 'Brands':
      fragment = (
        <FollowedBrands
          key={Date.now()}
          userData={userData}
          followings={followings}
        />
      );
      break;
    default:
      fragment = null;
  }

  return <div className="profile-fragment">{fragment}</div>;
};

export default ProfileFragement;
