import React from 'react';

const FollowedBrandDetails = ({ userData, selectecBrand }) => {
  const SCORE_LIST = [
    {
      title: 'Products',
      score: 7.1,
      subTitle: `How Does ${userData?.name} Rank There Products?`,
    },
    {
      title: 'People',
      score: 7.1,
      subTitle: `How Does ${userData?.name} Rank There Leadership?`,
    },
    {
      title: 'Profits',
      score: 7.1,
      subTitle: `How Does ${userData?.name} Rank There Earning Potential?`,
    },
  ];

  return (
    <div className="followed-brand-details">
      <div className="header-container">
        <img
          src={selectecBrand?.bankerDetails?.profilePicURL || ''}
          alt=""
          className="brand-icon"
        />
        <div className="brand-details">
          <div className="brand-name">
            {selectecBrand?.bankerDetails?.displayName || ''}
          </div>
          <div className="brand-tagline">
            How Does {userData?.name} Rank The{' '}
            {selectecBrand?.bankerDetails?.displayName || ''}?
          </div>
        </div>
        <div className="brand-score">8.1</div>
      </div>
      <div className="score-list">
        {SCORE_LIST.map(item => (
          <div key={item.title} className="score-item-container">
            <div className="score-details">
              <div className="score-title">{item.title}</div>
              <div className="score-tagline">{item.subTitle}</div>
            </div>
            <div className="score-count">{item.score}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default FollowedBrandDetails;
