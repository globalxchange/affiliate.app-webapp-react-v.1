import React, { useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import FollowedBrandDetails from './FollowedBrandDetails';

const FollowedBrands = ({ userData, followings }) => {
  const [selectecBrand, setSelectecBrand] = useState();

  if (selectecBrand) {
    return (
      <FollowedBrandDetails userData={userData} selectecBrand={selectecBrand} />
    );
  }

  return (
    <div className="row followed-brands">
      {followings
        ? followings?.map((item, index) => (
            <div
              key={index + item.title}
              className="col-4 brand-item"
              onClick={() => setSelectecBrand(item)}
            >
              <div className="brand-icon-container">
                <img
                  src={item?.bankerDetails?.profilePicURL || ''}
                  alt=""
                  className="brand-icon"
                />
              </div>
              <div className="brand-name">
                {item?.bankerDetails?.displayName || ''}
              </div>
            </div>
          ))
        : Array(6)
            .fill(1)
            .map((_, index) => (
              <div key={index} className="col-4 brand-item">
                <div className="brand-icon-container">
                  <Skeleton className="brand-icon" />
                </div>
                <Skeleton
                  className="brand-name"
                  count={1}
                  height={12}
                  width={80}
                />
              </div>
            ))}
    </div>
  );
};

export default FollowedBrands;

const BRANDS = [
  {
    title: 'Vault',
    icon: require('../../assets/images/liquid-icon.png').default,
  },
  {
    title: 'Agency',
    icon: require('../../assets/images/agency-icon-small.png').default,
  },
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/images/liquid-earnings-icon.png').default,
  },
  {
    title: 'Vault',
    icon: require('../../assets/images/liquid-icon.png').default,
  },
  {
    title: 'Agency',
    icon: require('../../assets/images/agency-icon-small.png').default,
  },
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/images/liquid-earnings-icon.png').default,
  },
  {
    title: 'Vault',
    icon: require('../../assets/images/liquid-icon.png').default,
  },
  {
    title: 'Agency',
    icon: require('../../assets/images/agency-icon-small.png').default,
  },
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/images/liquid-earnings-icon.png').default,
  },
];
