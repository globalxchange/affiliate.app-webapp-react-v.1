import React, { useState } from 'react'
import Navbar from "../NavItem/index"
import "./Home.scss"
import inlineheight from "../../assets/images/inlineheight.svg"
import colorArrow from "../../assets/images/colorArrow.svg"
import Banner from "../Banner"
import mobileData from "../../assets/images/mobileData.svg"

import myaffiliate from "../../assets/images/myaffiliate.svg"
import mobileversion from "../../assets/images/mobile-version.svg"
import arrow from "../../assets/images/arrow-affilate.svg"
import andriod from "../../assets/images/andriod.svg"
import ios from "../../assets/images/ios.svg"
import Aboutaffilate from "../../assets/images/about-affilate.svg";
import "aos/dist/aos.css";
export default function index() {
    const [info, setinfo] = useState("")
    return (
        <>
            <Banner data-aos="fade-down" />
            <Navbar data-aos="fade-down" />
            <div className='AboutDashboard'>
                <div className='Network'>
                    <h1 data-aos="fade-down" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">Build Your Network</h1>
                    <h3 data-aos="fade-down" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">Earn Money Each Time They</h3>
                    <div className='bottom-section'>
                        <div data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" className='Contain-section-about' onMouseLeave={() => setinfo("")} onMouseOver={() => setinfo("Buy Something")}>
                            <h4 style={info === "Buy Something" ? { fontWeight: "600" } : { opacity: "1" }}>Buy Something</h4>
                            {
                                info === "Buy Something" ?
                                    <img src={colorArrow} />
                                    :
                                    <img src={inlineheight} />
                            }


                        </div>
                        <div data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" className='Contain-section-about' onMouseLeave={() => setinfo("")} onMouseOver={() => setinfo("Start A Business")}>
                            <h4 style={info === "Start A Business" ? { fontWeight: "600" } : { opacity: "1" }}>Start A Business</h4>
                            {
                                info === "Start A Business" ?
                                    <img src={colorArrow} />
                                    :
                                    <img src={inlineheight} />
                            }

                        </div>

                        <div data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" className='Contain-section-about' onMouseLeave={() => setinfo("")} onMouseOver={() => setinfo("Exchange Assets")}>
                            <h4 style={info === "Exchange Assets" ? { fontWeight: "600" } : { opacity: "1" }}>Exchange Assets</h4>
                            {
                                info === "Exchange Assets" ?
                                    <img src={colorArrow} />
                                    :
                                    <img src={inlineheight} />
                            }

                        </div>
                        <div data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" className='Contain-section-about' onMouseLeave={() => setinfo("")} onMouseOver={() => setinfo("Lend Money")}>
                            <h4 style={info === "Lend Money" ? { fontWeight: "600" } : { opacity: "1" }}>Lend Money</h4>
                            {
                                info === "Lend Money" ?
                                    <img src={colorArrow} />
                                    :
                                    <img src={inlineheight} />
                            }

                        </div>
                        <div data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" className='Contain-section-about' onMouseLeave={() => setinfo("")} onMouseOver={() => setinfo("Sell Something")}>
                            <h4 style={info === "Sell Something" ? { fontWeight: "600" } : { opacity: "1" }}>Sell Something</h4>
                            {
                                info === "Sell Something" ?
                                    <img src={colorArrow} />
                                    :
                                    <img src={inlineheight} />
                            }

                        </div>
                        <div data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" className='Contain-section-about' onMouseLeave={() => setinfo("")} onMouseOver={() => setinfo("Makes An Investment")}>
                            <h4 style={info === "Makes An Investment" ? { fontWeight: "600" } : { opacity: "1" }}>Makes An Investment</h4>
                            {
                                info === "Makes An Investment" ?
                                    <img src={colorArrow} />
                                    :
                                    <img src={inlineheight} />
                            }

                        </div>
                    </div>

                </div>
                <div className='right_image' data-aos="fade-left">
                    <img className="desktop-image" src={mobileData} />
                    <img className='mobile-image' src={mobileversion} />
                </div>
            </div>
            <div className='bottomBanner'  >
                <div className='text-area' >
                    <h6>Download The Beta Version Today </h6>

                    <div className='image-section'>
                        <img src={arrow} />
                        <img src={arrow} />
                        <img src={arrow} />
                    </div>

                </div>
                <div className='store'>
                    <img className='paystore' src={andriod}></img>
                    <div className='line-affilate'></div>
                    <img className='paystore' src={ios}></img>
                </div>
            </div>
            <div className='affilate-about-dash'>
                <div className='affilate-about'>
                    {/* <div className='rightside-affilate-image'>
</div> */} <img src={Aboutaffilate} data-aos="zoom-in" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600" />
                    <div className='right' data-aos="zoom-in" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
                        <h1>What Is Affiliate App?</h1>

                        <p>Affiliate App is the world’s first socialfi (social + finance) platform which allows you to earn money from all the people within your affiliate network. Everytime a person in your network buys a product or transacts with one of the brands listed on Affiliate App, you get a cut.</p>
                    </div>
                </div>
                <div className='affilate-card'>
                    <div className='background-affilate'></div>
                    <div className='list' data-aos="flip-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
                        <div className="letter">
                            <h1>1</h1>
                        </div>

                        <h3>Create Your Account</h3>
                        <p>Get Started In less than 5 minutes as long as you are invited by an existing affiliate. Active Your Account By Purchasing The Subscription.</p>
                    </div>
                    <div className='list' data-aos="flip-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
                        <div className="letter">
                            <h1>2</h1>
                        </div>
                        <h3>Build Your Network </h3>
                        <p>Start adding friends to your network by inviting them to join the Affiliate App with your specific username.</p>
                    </div>
                    <div className='list' data-aos="flip-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
                        <div className="letter">
                            <h1>3</h1>
                        </div>
                        <h3>Get Paid</h3>
                        <p>Promote the brands that are listed on the Affiliate App to your newly built network and generate income each time they transact.</p>
                    </div>

                </div>
            </div>
    
 
            <div className='video-section'>
            <iframe
      className='iframe-video'
      src={`https://www.youtube.com/embed/BNPSyW1DvDY`}
      frameBorder="0"

      allowFullScreen
      title="Embedded youtube"
    />
    <h1>Are You Looking To Be An <img src={myaffiliate}/> ? </h1>
<h3>
Affiliate App is the world’s first socialfi (social + finance) platform which allows you to earn money from all the people within your affiliate network. 
    
     </h3>
            </div>
        </>

    )
}
