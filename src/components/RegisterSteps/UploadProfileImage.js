import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

const UploadProfileImage = ({ onNext }) => {
  const [selectedImage, setSelectedImage] = useState();
  const [imageLocalURI, setImageLocalURI] = useState();

  useEffect(() => {
    if (selectedImage) {
      setImageLocalURI(URL.createObjectURL(selectedImage));
    } else {
      setImageLocalURI();
    }
  }, [selectedImage]);

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedImage(undefined);
      return;
    }

    setSelectedImage(e.target.files[0]);
  };

  const onNextClick = () => {
    if (!selectedImage) {
      return toast.error('PLEASE SELECT A PROFILE IMAGE');
    }

    onNext();
  };

  return (
    <div className="register-component">
      <h1 className="header">Congrats</h1>
      <h6 className="sub-header">
        You&lsquo;re Almost Done. Just Upload A Profile Picture To Seal The Deal
      </h6>
      <div className="form-container">
        <div className="profile-image-container">
          <img
            className="profile-image"
            src={
              imageLocalURI ||
              require('../../assets/images/user-filled.svg').default
            }
            alt=""
          />
          <input
            className="image-uploader"
            type="file"
            onChange={onSelectFile}
          />
        </div>
      </div>
      <div onClick={onNextClick} className="next-btn">
        Next
      </div>
    </div>
  );
};

export default UploadProfileImage;
