import React from 'react';
import { toast } from 'react-toastify';
import { onPaste } from '../../utils';

const BrokerCodeInput = ({ setBrokerSyncCode, brokerSyncCode, onNext }) => {
  const onNextClick = () => {
    if (!brokerSyncCode) {
      toast.error('Please Enter The Broker Code');
    } else {
      onNext();
    }
  };

  return (
    <div className="register-component">
      {/* <h1 className="header">BrokerSync</h1> */}
      <h6 className="sub-header">
        Please Enter The BrokerSync Code From Your Broker
      </h6>
      <div className="form-container">
        <div className="input-container">
          <input
            type="text"
            className="input-field"
            placeholder="Enter Code.."
            value={brokerSyncCode}
            onChange={(e) => setBrokerSyncCode(e.target.value)}
          />
          <div
            onClick={() => onPaste(setBrokerSyncCode)}
            className="input-action"
          >
            <img
              src={require('../../assets/images/copy-icon-colored.svg').default}
              alt=""
              className="input-action-icon"
            />
          </div>
        </div>
      </div>
      <div onClick={onNextClick} className="next-btn">
        Next
      </div>
    </div>
  );
};

export default BrokerCodeInput;
