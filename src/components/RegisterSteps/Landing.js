import React from 'react';

const Landing = ({ setUserKnownFrom, onNext }) => {
  const itemPress = (item) => {
    setUserKnownFrom(item);
    onNext();
  };

  return (
    <div className="register-component">
      <h1 className="header">Register</h1>
      <h6 className="sub-header">
        Before You Start. We Need To Know How You Found Out About Us
      </h6>
      <div className="options-list">
        {LIST.map((item) => (
          <div
            onClick={() => itemPress(item)}
            className="option-item"
            key={item.title}
          >
            <img className="option-icon" src={item.icon} alt="" />
            <div className="option-name">{item.title}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Landing;

const LIST = [
  {
    title: 'BROKER',
    icon: require('../../assets/images/affliate-app-white-logo.svg').default,
  },
];
