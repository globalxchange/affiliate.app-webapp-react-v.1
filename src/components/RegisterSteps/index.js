import Axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { APP_CODE, GX_API_ENDPOINT, GX_AUTH_URL } from '../../configs';
import BrokerCodeInput from './BrokerCodeInput';
import CredentialsForm from './CredentialsForm';
import CryptoJS from 'crypto-js';
import PasswordForm from './PasswordForm';
import OTPInputForm from './OTPInputForm';
import LoadingAnimation from '../LoadingAnimation';
import UploadProfileImage from './UploadProfileImage';
import { toast } from 'react-toastify';
import LocalStorageHelper from '../../utils/LocalStorageHelper';
import { withRouter } from 'react-router-dom';
import { BrokerAppContext } from '../../contexts/BrokerAppContext';
import RegistrationSuccess from './RegistrationSuccess';

const RegisterSteps = ({ history, isLoading, setIsLoading, appCode }) => {
  const { setRefreshData } = useContext(BrokerAppContext);

  const [activeStep, setActiveStep] = useState();
  const [usersList, setUsersList] = useState();

  const [brokerSyncCode, setBrokerSyncCode] = useState('');
  const [emailId, setEmailId] = useState('');
  const [userName, setUserName] = useState('');
  const [passwordInput, setPasswordInput] = useState('');
  const [otpInput, setOtpInput] = useState('');

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/listUsernames`).then((res) => {
      const { data } = res;
      if (data.status) {
        const bytes = CryptoJS.Rabbit.decrypt(data.payload, 'gmBuuQ6Er8XqYBd');
        const jsonString = bytes.toString(CryptoJS.enc.Utf8);
        const resultObj = JSON.parse(jsonString);
        setUsersList(resultObj);
      }
    });
  }, []);

  const sendVerificationCode = () => {
    const postData = {
      username: userName.toLowerCase().trim(),
      email: emailId.toLowerCase().trim(),
      password: passwordInput.trim(),
      ref_affiliate: brokerSyncCode || '1',
      account_type: 'Personal',
      signedup_app: appCode || APP_CODE,
    };
    // setActiveRegStep('VERIFY');

    // console.log('PostData', postData);

    setIsLoading(true);

    Axios.post(`${GX_AUTH_URL}/gx/user/signup`, postData)
      .then((resp) => {
        const { data } = resp;

        // console.log('Response', data);

        if (data.status) {
          setActiveStep('OTPInput');
        } else {
          toast.error(data.message);
        }
      })
      .catch((error) =>
        console.log('Error on sending Verification Code', error),
      )
      .finally(() => setIsLoading(false));
  };

  const resendVerificationCode = () => {
    setIsLoading(true);

    const postData = {
      email: emailId.toLowerCase().trim(),
    };

    Axios.post(`${GX_AUTH_URL}/gx/user/confirm/resend`, postData)
      .then((resp) => {
        const { data } = resp;
        // console.log('Resend Code', data);
        toast.warn(data.message);
      })
      .catch((error) =>
        console.log('Error on resending verification code', error),
      )
      .finally(() => setIsLoading(false));
  };

  const verifyOTP = () => {
    if (otpInput.length < 6) {
      return toast.error('Please Enter The Six Digit Verification Code');
    }

    setIsLoading(true);

    const email = emailId.toLowerCase().trim();

    const postData = {
      email,
      code: otpInput.toString().trim(),
    };
    Axios.post(`${GX_AUTH_URL}/gx/user/confirm`, postData)
      .then((resp) => {
        const { data } = resp;

        // console.log('VerifyResp', data);

        if (data.status) {
          // loginUser();
          setActiveStep('Success');
        } else {
          toast.error(data.message);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        console.log('Error on verifying OTP', error);
        setIsLoading(false);
      });
  };

  const loginUser = () => {
    const email = emailId.toLowerCase().trim();

    setIsLoading(true);

    Axios.post('https://gxauth.apimachine.com/gx/user/login', {
      email,
      password: passwordInput,
    })
      .then((response) => {
        const { data } = response;
        if (data.status) {
          toast.success('✅ Logged in...');
          Axios.post(`${GX_API_ENDPOINT}/gxb/apps/register/user`, {
            email,
            app_code: appCode || APP_CODE,
          }).then((profileResp) => {
            // console.log('profileResp', profileResp.data);

            if (profileResp.data.profile_id) {
              LocalStorageHelper.setProfileId(profileResp.data.profile_id);
              setRefreshData();
            }
          });
          Axios.post(`${GX_API_ENDPOINT}/get_affiliate_data`, {
            email,
          })
            .then((res) => {
              LocalStorageHelper.setUserDetails(
                res.data.name,
                `0x${res.data.ETH_Address}`,
                res.data.affiliate_id,
              );
            })
            .catch((error) => {
              console.log('getUserDetails Error', error);
            })
            .finally(() => {
              LocalStorageHelper.setAppLoginData(
                data.idToken,
                data.accessToken,
                email,
              );
              setIsLoading(false);
              history.push('/');
            });
        } else {
          toast.error(`❌ ${data.message}`);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log('Login Error', error);
        setIsLoading(false);
      })
      .finally(() => {});
  };

  if (isLoading) {
    return (
      <div className="d-flex h-100 flex-fill justify-content-center align-items-center">
        <LoadingAnimation dark />
      </div>
    );
  }

  switch (activeStep) {
    case 'BrokerCode':
      return (
        <BrokerCodeInput
          setBrokerSyncCode={setBrokerSyncCode}
          brokerSyncCode={brokerSyncCode}
          onNext={() => setActiveStep('Credentials')}
        />
      );

    case 'Credentials':
      return (
        <CredentialsForm
          emailId={emailId}
          setEmailId={setEmailId}
          userName={userName}
          setUserName={setUserName}
          onNext={() => setActiveStep('PasswordForm')}
          usersList={usersList}
        />
      );

    case 'PasswordForm':
      return (
        <PasswordForm
          passwordInput={passwordInput}
          setPasswordInput={setPasswordInput}
          onNext={() => sendVerificationCode()}
        />
      );

    case 'OTPInput':
      return (
        <OTPInputForm
          otpInput={otpInput}
          setOtpInput={setOtpInput}
          onNext={() => verifyOTP()}
          resendVerificationCode={resendVerificationCode}
        />
      );

    case 'Success':
      return <RegistrationSuccess />;

    case 'UploadImage':
      return <UploadProfileImage onNext={() => {}} />;

    default:
      return (
        <BrokerCodeInput
          setBrokerSyncCode={setBrokerSyncCode}
          brokerSyncCode={brokerSyncCode}
          onNext={() => setActiveStep('Credentials')}
        />
      );
  }
};

export default withRouter(RegisterSteps);
