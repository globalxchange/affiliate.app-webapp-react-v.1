import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { emailValidator, usernameRegex } from '../../utils';

const CredentialsForm = ({
  emailId,
  setEmailId,
  userName,
  setUserName,
  usersList,
  onNext,
}) => {
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isUserNameValid, setIsUserNameValid] = useState(false);

  useEffect(() => {
    if (usersList) {
      const email = emailId.toLowerCase().trim();
      const validEmail = emailValidator(email);

      if (!validEmail) {
        return setIsEmailValid(false);
      }

      setIsEmailValid(!usersList.emails.includes(email));
    }
    return () => {};
  }, [emailId, usersList]);

  useEffect(() => {
    if (usersList) {
      const username = userName.toLowerCase().trim();

      if (username.length < 6 || !usernameRegex.test(username)) {
        return setIsUserNameValid(false);
      }

      setIsUserNameValid(!usersList.usernames.includes(username));
    }
    return () => {};
  }, [userName, usersList]);

  const onNextClick = () => {
    const email = emailId.toLowerCase().trim();
    if (!isEmailValid) {
      if (!emailValidator(email)) {
        return toast.error('PLEASE INPUT A VALID EMAIL ID');
      }
      return toast.error('EMAIL ID ALREADY IN USE');
    }

    const username = userName.toLowerCase().trim();

    if (!isUserNameValid) {
      if (!usernameRegex.test(username)) {
        return toast.error('PLEASE INPUT A VALID USERNAME');
      }
      return toast.error('USERNAME ALREADY IN USE');
    }

    onNext();
  };

  return (
    <div className="register-component">
      {/* <h1 className="header">Register</h1> */}
      {/* <h6 className="sub-header">Step 1: Email & Username</h6> */}
      <div className="form-container">
        <div className="input-container">
          <div
            className="input-action"
            style={{ marginLeft: 0, marginRight: 10 }}
          >
            {/* <img
              src={require('../../assets/images/user-colored.svg').default}
              alt=""
              className="input-action-icon"
            /> */}
          </div>
          <input
            type="email"
            className="input-field"
            placeholder="Create Broker Username"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
          <div
            className="validator"
            style={{ backgroundColor: isUserNameValid ? 'green' : '#d80027' }}
          />
        </div>
        <div className="input-container">
          <div
            className="input-action"
            style={{ marginLeft: 0, marginRight: 10 }}
          >
            {/* <img
              src={require('../../assets/images/email-icon.svg').default}
              alt=""
              className="input-action-icon"
            /> */}
          </div>
          <input
            type="email"
            className="input-field"
            placeholder="Enter Email"
            value={emailId}
            onChange={(e) => setEmailId(e.target.value)}
          />
          <div
            className="validator"
            style={{ backgroundColor: isEmailValid ? 'green' : '#d80027' }}
          />
        </div>
      </div>
      <div onClick={onNextClick} className="next-btn">
        Next
      </div>
    </div>
  );
};

export default CredentialsForm;
