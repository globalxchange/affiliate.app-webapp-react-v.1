import React from 'react';
import OtpInput from 'react-otp-input';
import { toast } from 'react-toastify';

const OTPInputForm = ({
  otpInput,
  setOtpInput,
  onNext,
  resendVerificationCode,
}) => {
  const onNextClick = () => {
    if (otpInput.length < 6) {
      return toast.error('PLEASE INPUT THE OTP');
    }
    onNext();
  };

  return (
    <div className="register-component">
      {/* <h1 className="header">Register</h1> */}
      <h6 className="sub-header">Enter The Six Digit Number From Your Email</h6>
      <div className="form-container">
        <div className="input-container border-0">
          <OtpInput
            value={otpInput}
            onChange={otp => setOtpInput(otp)}
            numInputs={6}
            containerStyle="otp-container"
            inputStyle="input-field otp-input"
            shouldAutoFocus
          />
        </div>
        <div onClick={resendVerificationCode} className="resend-btn">
          Resend OTP
        </div>
      </div>
      <div onClick={onNextClick} className="next-btn">
        Next
      </div>
    </div>
  );
};

export default OTPInputForm;
