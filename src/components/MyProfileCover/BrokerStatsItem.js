import React from 'react';
import starIcon from '../../assets/images/star-icon.svg';

const BrokerStatsItem = ({ data }) => {
  return (
    <div className={`broker-stats ${data.up ? 'up' : ''}`}>
      <img src={starIcon} alt="" />
      <h6 className="stats-item-title">{data.title}</h6>
      <h6 className="stats-item-value">{data.value}</h6>
    </div>
  );
};

export default BrokerStatsItem;
