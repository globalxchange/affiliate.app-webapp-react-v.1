import React from 'react';

const ChainViewHeader = ({
  headerDesc,
  level,
  onEdit,
  showEdit,
  showSearch,
  onSearch,
}) => {
  return (
    <div className="chain-view-header">
      <div className="chain-view-level">DD {level || 0}</div>
      <div className="chian-view-title">{headerDesc}</div>
      {showEdit && (
        <div className="action-item" onClick={onEdit}>
          <img
            src={require('../../assets/images/edit-icon.svg').default}
            alt=""
            className="action-icon"
          />
        </div>
      )}
      {showSearch && (
        <div className="action-item" onClick={onSearch}>
          <img
            src={require('../../assets/images/search-colorful.svg').default}
            alt=""
            className="action-icon"
          />
        </div>
      )}
      <div className="action-item">
        <img
          src={require('../../assets/images/network-people.svg').default}
          alt=""
          className="action-icon"
        />
      </div>
    </div>
  );
};

export default ChainViewHeader;
