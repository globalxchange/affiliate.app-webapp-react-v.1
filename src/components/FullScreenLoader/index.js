import React from 'react';

const FullScreenLoader = () => {
  return (
    <div className="full-screen-loader">
      <div className="full-loading-wrapper">
        <div className="full-loader">
          <div />
          <div>
            <div />
          </div>
        </div>
      </div>
    </div>
  );
};

export default FullScreenLoader;
