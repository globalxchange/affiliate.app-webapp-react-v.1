import React from 'react';
import Slider from 'react-slick';

const MobileHomeCarousel = ({ items }) => {
  return (
    <div className="container-fluid mobile-carousel px-0">
      <Slider
        infinite
        speed={500}
        slidesToShow={1}
        slidesToScroll={1}
        centerMode
        useTransform
        useCSS
      >
        {items.map(item => (
          <div key={item.title} className="mobile-item">
            <img src={item.image} alt="" className="item-image" />
            <div className="item-button">{item.title}</div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default MobileHomeCarousel;
