import React, { useContext, useEffect } from 'react';
import DirectDigitalRevenueHeader from '../../TableHeaders/DirectDigitalRevenueHeader';
import DirectDigitalRevenueItem from '../../TableItems/DirectDigitalRevenueItem';
import { CRMContext } from '../../../../contexts/CRMContext';

const IndirectDigitalTransactionalVolume = () => {
  const {
    indirectDigitalVolume,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(indirectDigitalVolume);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <DirectDigitalRevenueHeader />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <DirectDigitalRevenueItem key={index} data={item} />
      ))}
    </>
  );
};

export default IndirectDigitalTransactionalVolume;
