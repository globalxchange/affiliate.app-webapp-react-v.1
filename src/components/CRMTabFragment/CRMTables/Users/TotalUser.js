import React, { useEffect, useContext } from 'react';
import { CRMContext } from '../../../../contexts/CRMContext';
import TotalUsersHeaders from '../../TableHeaders/TotalUsersHeaders';
import TotalUserItem from '../../TableItems/TotalUserItem';

const TotalUser = () => {
  const {
    totalUsersList,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(totalUsersList);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <TotalUsersHeaders />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <TotalUserItem key={index} data={item} />
      ))}
    </>
  );
};

export default TotalUser;
