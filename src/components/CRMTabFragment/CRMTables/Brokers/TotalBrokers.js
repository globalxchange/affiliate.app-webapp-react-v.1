import React, { useContext, useEffect } from 'react';
import { CRMContext } from '../../../../contexts/CRMContext';
import TotalUsersHeaders from '../../TableHeaders/TotalUsersHeaders';
import TotalUserItem from '../../TableItems/TotalUserItem';

const TotalBrokers = () => {
  const {
    totalBrokerList,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(totalBrokerList);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <TotalUsersHeaders />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <TotalUserItem key={index} data={item} />
      ))}
    </>
  );
};

export default TotalBrokers;
