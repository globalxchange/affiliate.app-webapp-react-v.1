import React, { useContext, useEffect } from 'react';
import { CRMContext } from '../../../../contexts/CRMContext';
import IndirectOTCRevenueHeader from '../../TableHeaders/IndirectOTCRevenueHeader';
import IndirectOTCRevenueItem from '../../TableItems/IndirectOTCRevenueItem';

const IndirectOTCTransactionalRevenue = () => {
  const {
    indirectOtcRevenue,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(indirectOtcRevenue);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <IndirectOTCRevenueHeader />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <IndirectOTCRevenueItem key={index} data={item} />
      ))}
    </>
  );
};

export default IndirectOTCTransactionalRevenue;
