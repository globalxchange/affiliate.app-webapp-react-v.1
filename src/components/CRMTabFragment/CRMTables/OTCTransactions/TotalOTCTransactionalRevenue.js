import React, { useContext, useEffect } from 'react';
import { CRMContext } from '../../../../contexts/CRMContext';
import TotalOTCRevenueHeader from '../../TableHeaders/TotalOTCRevenueHeader';
import TotalOTCRevenueItem from '../../TableItems/TotalOTCRevenueItem';

const TotalOTCTransactionalRevenue = () => {
  const {
    totalOtcRevenue,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(totalOtcRevenue);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <TotalOTCRevenueHeader />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <TotalOTCRevenueItem key={index} data={item} />
      ))}
    </>
  );
};

export default TotalOTCTransactionalRevenue;
