import React, { useEffect, useContext } from 'react';
import { CRMContext } from '../../../../contexts/CRMContext';
import DirectTokenTransactionalHeader from '../../TableHeaders/DirectTokenTransactionalHeader';
import DirectTokenTransactionalItem from '../../TableItems/DirectTokenTransactionalItem';

const DirectTokenTransactionalRevenue = () => {
  const {
    directTokenTxnRevenue,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(directTokenTxnRevenue);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <DirectTokenTransactionalHeader />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <DirectTokenTransactionalItem key={index} data={item} />
      ))}
    </>
  );
};

export default DirectTokenTransactionalRevenue;
