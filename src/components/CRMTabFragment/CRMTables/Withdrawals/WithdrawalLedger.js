import React, { useContext, useEffect } from 'react';
import { CRMContext } from '../../../../contexts/CRMContext';
import WithdrawalLedgerItem from '../../TableItems/WithdrawalLedgerItem';
import WithdrawalLedgerHeader from '../../TableHeaders/WithdrawalLedgerHeader';

const WithdrawalLedger = () => {
  const {
    withdrawals,
    setCurrentSets,
    currentDataSet,
    currentPage
  } = useContext(CRMContext);

  useEffect(() => {
    setCurrentSets(withdrawals);
  }, []);

  const postPerPage = 10;

  const offset = (currentPage - 1) * postPerPage;

  return (
    <>
      <WithdrawalLedgerHeader />
      {currentDataSet.slice(offset, offset + postPerPage).map((item, index) => (
        <WithdrawalLedgerItem key={index} data={item} />
      ))}
    </>
  );
};

export default WithdrawalLedger;
