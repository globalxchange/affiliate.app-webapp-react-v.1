import React from 'react';
import {
  getDateFromTimeStamp,
  getTimeFromTimeStamp,
  usdValueFormatter
} from '../../../utils';
import coin from '../../../assets/images/coin.svg';
import CADIcon from '../../../assets/images/cadIcon.png';

const IndirectTokenTransactionalItem = ({ data }) => {
  return !data.commissions ? (
    '<div></div>'
  ) : (
    <div id="details-table-item" className="details-table-item">
      <div className="flexed-item" style={{ width: '3%' }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: '28%', paddingLeft: '15px' }}
      >
        <label className="token-txn-name">{data.txn.name}</label>
      </div>

      <div className="flexed-item" style={{ width: '10%' }}>
        <img className="user-country-icon" src={CADIcon} alt="" />
      </div>
      <div className="flexed-item " style={{ width: '20%' }}>
        <label className="token-txn-date">
          {getDateFromTimeStamp(data.txn.timestamp)}
        </label>
        <label className="token-txn-time">
          {getTimeFromTimeStamp(data.txn.timestamp)}
        </label>
      </div>
      <div className="flexed-item" style={{ width: '25%' }}>
        <label className="token-txn-broker">{data.txn.upline_name}</label>
        <label className="token-txn-dd">DD : {data.commissions.ps}</label>
      </div>
      <div className="flexed-item" style={{ width: '14%' }}>
        <label className="token-txn-commission">
          {data.commissions.ps > 0
            ? usdValueFormatter.format(data.commissions.it_commission)
            : usdValueFormatter.format(data.commissions.dt_commission)}
        </label>
      </div>
    </div>
  );
};

export default IndirectTokenTransactionalItem;
