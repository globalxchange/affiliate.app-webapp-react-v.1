import React from 'react';
import coin from '../../../assets/images/coin.svg';
import CADIcon from '../../../assets/images/cadIcon.png';
import { getDateFromTimeStamp, getTimeFromTimeStamp } from '../../../utils';

const TotalUserItem = ({ data }) => {
  return (
    <div id="details-table-item" className="details-table-item">
      <div className="flexed-item" style={{ width: '3%' }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: '30%', paddingLeft: '15px' }}
      >
        <label className="user-name">{data.name}</label>
      </div>

      <div className="flexed-item" style={{ width: '10%' }}>
        {/* <label>{data.country ? data.country : "-"}</label> */}
        <img className="user-country-icon" src={CADIcon} alt="" />
      </div>
      <div className="flexed-item" style={{ width: '28%' }}>
        <label className="user-signup-date">
          {getDateFromTimeStamp(data.timestamp)}
        </label>
        <label className="user-signup-time">
          {getTimeFromTimeStamp(data.timestamp)}
        </label>
      </div>
      <div className="flexed-item" style={{ width: '25%' }}>
        <label className="user-broker-name">{data.broker_name}</label>
      </div>
      <div className="flexed-item" style={{ width: '35px' }} />
    </div>
  );
};

export default TotalUserItem;
