import React from 'react';
import coin from '../../../assets/images/coin.svg';
import {
  getTimeFromTimeStamp,
  getDateFromTimeStamp,
  formatToExpValue,
  formatFromExpValues
} from '../../../utils';

const TotalDigitalRevenueItem = ({ data }) => {
  return (
    <div id="details-table-item" className="details-table-item">
      <div className="flexed-item" style={{ width: '3%' }}>
        <img src={coin} alt="" height="30px" />
      </div>
      <div
        className="flexed-item"
        style={{ width: '24%', paddingLeft: '15px' }}
      >
        <label className="token-txn-name">{data.txn.name}</label>
      </div>

      <div className="flexed-item" style={{ width: '18%' }}>
        <label className="token-txn-date">
          {getDateFromTimeStamp(data.txn.timestamp)}
        </label>
        <label className="token-txn-time">
          {getTimeFromTimeStamp(data.txn.timestamp)}
        </label>
      </div>
      <div className="flexed-item" style={{ width: '12%' }}>
        <label className="token-txn-date">{data.txn.coin}</label>
      </div>
      <div className="flexed-item" style={{ width: '22%' }}>
        <label className="token-txn-commission" style={{ fontSize: '1.4rem' }}>
          {data.txn.earned_usd_value
            ? `$ ${formatToExpValue(data.txn.earned_usd_value)}`
            : '$ 0'}
        </label>
      </div>
      <div className="flexed-item" style={{ width: '18%' }}>
        <label className="token-txn-commission" style={{ fontSize: '1.4rem' }}>
          {data.txn.commissions_value
            ? `$ ${formatFromExpValues(data.txn.commissions_value)}`
            : '$ 0'}
        </label>
      </div>
      <div className="flexed-item" style={{ width: '35px' }} />
    </div>
  );
};

export default TotalDigitalRevenueItem;
