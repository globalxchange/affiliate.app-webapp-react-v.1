import React, { useContext } from 'react';
import { CRMContext } from '../../../contexts/CRMContext';
import FullScreenToggler from '../FullScreenToggler';
import TableSearchBar from '../TableSearchBar';

const WithdrawalLedgerHeader = () => {
  const { isTableSearchOpen, isTableExpanded, expandTable } = useContext(
    CRMContext
  );

  return (
    <div className="details-table-item details-table-header scroll-shadow">
      {!isTableSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{
              width: '3%'
            }}
          />
          <div
            className="flexed-item"
            style={{
              width: '22%',
              paddingLeft: '15px'
            }}
          >
            <label>Method</label>
          </div>

          <div
            className="flexed-item"
            style={{
              width: '14%'
            }}
          >
            <label>Date(EST)</label>
          </div>
          <div
            className="flexed-item"
            style={{
              width: '14%'
            }}
          >
            <label>USD Amount</label>
          </div>

          <div
            className="flexed-item"
            style={{
              width: '15%'
            }}
          >
            <label>Value</label>
          </div>
          <div
            className="flexed-item"
            style={{
              width: '16%'
            }}
          >
            <label>Balance</label>
          </div>
          <div
            className="flexed-item"
            style={{
              width: '16%'
            }}
          >
            <label>Status</label>
          </div>
        </>
      ) : (
        <div
          className="flexed-item"
          style={{
            width: '65%'
          }}
        />
      )}

      <FullScreenToggler
        isExpanded={isTableExpanded}
        expandTable={expandTable}
      />
      <TableSearchBar />
    </div>
  );
};

export default WithdrawalLedgerHeader;
