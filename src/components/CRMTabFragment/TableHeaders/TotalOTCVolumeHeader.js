import React, { useContext } from 'react';
import FullScreenToggler from '../FullScreenToggler';
import TableSearchBar from '../TableSearchBar';
import { CRMContext } from '../../../contexts/CRMContext';

const TotalOTCVolumeHeader = () => {
  const { isTableSearchOpen, isTableExpanded, expandTable } = useContext(
    CRMContext
  );
  return (
    <div className="details-table-item details-table-header scroll-shadow">
      {!isTableSearchOpen ? (
        <>
          <div
            className="flexed-item"
            style={{
              width: '3%'
            }}
          />
          <div
            className="flexed-item"
            style={{
              width: '25%',
              paddingLeft: '15px'
            }}
          >
            <label>Name</label>
          </div>

          <div
            className="flexed-item"
            style={{
              width: '8%'
            }}
          >
            <label>Country</label>
          </div>
          <div
            className="flexed-item"
            style={{
              width: '18%'
            }}
          >
            <label>Date(EST)</label>
          </div>
          <div
            className="flexed-item"
            style={{
              width: '24%'
            }}
          >
            <label>Upline / DD</label>
          </div>
          <div
            className="flexed-item"
            style={{
              width: '17%'
            }}
          >
            <label>TXN Volume</label>
          </div>
        </>
      ) : (
        <div
          className="flexed-item"
          style={{
            width: '65%'
          }}
        />
      )}
      <FullScreenToggler
        isExpanded={isTableExpanded}
        expandTable={expandTable}
      />
      <TableSearchBar />
    </div>
  );
};

export default TotalOTCVolumeHeader;
