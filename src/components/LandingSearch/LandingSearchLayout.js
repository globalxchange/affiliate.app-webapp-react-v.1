/* eslint-disable react-hooks/exhaustive-deps */
import axios from 'axios';
import React, {
  useContext,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from 'react';
import Skeleton from 'react-loading-skeleton';
import { APP_CODE, GX_API_ENDPOINT } from '../../configs';
import ProfileAvatar from '../ProfileAvatar';
import UserSelectedView from './UserSelectedView';
import { FixedSizeList as List } from 'react-window';
import { useHistory, useLocation } from 'react-router-dom';
import { BrokerAppContext } from '../../contexts/BrokerAppContext';

const LandingSearchLayout = ({
  isSearchLayoutOpen,
  activeSearchOption,
  activeSearchFilter,
  onBack,
  searchText,
  setSearchText,
  openOptionPopup,
  openFilterPopup,
  isSearchAsList,
  toggleSearchType,
}) => {
  const history = useHistory();
  const location = useLocation();

  const { searchNearByItems } = useContext(BrokerAppContext);

  const inputRef = useRef();
  const axiosCancelRef = useRef();
  const scrollRef = useRef();

  const [affilateList, setAffilateList] = useState();
  const [searchList, setSearchList] = useState([]);
  const [noOfItemPerPage, setNoOfItemPerPage] = useState(PAGE_COUNTS[0]);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [isUserViewOpened, setIsUserViewOpened] = useState(false);
  const [selectedUser, setSelectedUser] = useState();
  const [listHeight, setListHeight] = useState();

  useLayoutEffect(() => {
    if (scrollRef.current) {
      const height = scrollRef.current.clientHeight;
      setListHeight(height);
    }
  }, [
    scrollRef.current,
    scrollRef.current ? scrollRef.current.clientHeight : 0,
  ]);

  useEffect(() => {
    if (isSearchLayoutOpen) {
      inputRef?.current && inputRef.current.focus();
    } else {
      setIsUserViewOpened(false);
    }
  }, [isSearchLayoutOpen]);

  useEffect(() => {
    if (searchText.trim()) {
      setIsUserViewOpened(false);
    } else {
      setCurrentPage(1);
    }
  }, [searchText]);

  useEffect(() => {
    setCurrentPage(1);
  }, [noOfItemPerPage]);

  useEffect(() => {
    if (isUserViewOpened) {
      setSearchText('');
    }
  }, [isUserViewOpened]);

  useEffect(() => {
    if (activeSearchOption?.title === 'Affiliates' && !isUserViewOpened) {
      if (axiosCancelRef.current) {
        axiosCancelRef.current.cancel('Cancelled');
      }

      const searchQuery = searchText.trim().toLowerCase();

      setAffilateList();
      axiosCancelRef.current = axios.CancelToken.source();

      let params = {
        // app_code: APP_CODE,
        downlines_count: true,
      };

      if (!isSearchAsList) {
        params[`${activeSearchFilter.key}`] = searchQuery;
      } else if (searchQuery) {
        params = {
          ...params,
          limit: 9999999,
          searchQuery,
          searchParam: activeSearchFilter.key,
        };
      } else {
        params = {
          ...params,
          limit: noOfItemPerPage.count,
          skip: noOfItemPerPage.count * (currentPage - 1),
        };
      }

      // console.log('Params', params);

      axios
        .get(`${GX_API_ENDPOINT}/coin/vault/service/users/holdings/data/get`, {
          params,
          cancelToken: axiosCancelRef.current.token,
        })
        .then(({ data }) => {
          // console.log('Search Data', data);
          if (isSearchAsList) {
            setTotalPages(
              Math.ceil((data.totalUsersCount || 0) / noOfItemPerPage.count),
            );

            const affList = data.users || [];
            const parsedList = affList.map((item) => ({
              ...item,
              name: item.userData?.name,
              profile_img: item.userData?.profile_img,
              email: item.userData?.email,
            }));
            setAffilateList(parsedList);
          } else {
            const affList = data.users || [];

            if (data.count === 1) {
              const user = affList[0];

              history.push(`/brokerid/${user?.userData?.username || ''}`, {
                selectedUser: {
                  ...user,
                  name: user.userData?.name,
                  profile_img: user.userData?.profile_img,
                  email: user.userData?.email,
                },
              });
            } else {
              setAffilateList([]);
            }
          }
        })
        .catch((error) => {
          // console.log('Error on getting affliate list', error);
        });
    } else if (axiosCancelRef.current) {
      axiosCancelRef.current.cancel('Cancelled');
    }
  }, [
    noOfItemPerPage,
    currentPage,
    activeSearchOption,
    searchText,
    activeSearchFilter,
    isSearchAsList,
  ]);

  useEffect(() => {
    if (scrollRef.current) {
      scrollRef.current.scrollTo(0, 0);
    }
  }, [affilateList, searchText]);

  useEffect(() => {
    if (activeSearchOption?.title !== 'Affiliates') {
      const { list } = activeSearchOption;
      if (list) {
        const searchQuery = searchText.trim().toLowerCase();

        const filter = list.filter(
          (x) =>
            x?.name?.trim().toLowerCase().includes(searchQuery) ||
            x?.email?.trim().toLowerCase().includes(searchQuery),
        );
        setSearchList(filter);
      } else {
        setSearchList();
      }
    } else {
      setSearchList(affilateList);
    }
  }, [activeSearchOption, searchText, affilateList]);

  useEffect(() => {
    if (location.state?.isOpenUser) {
      const user = location.state?.userToOpen;
      setIsUserViewOpened(true);
      setSelectedUser(user);
    }
  }, [location]);

  const updatePage = (isIncrement) => {
    if (isIncrement) {
      const nextPage = currentPage + 1;
      if (nextPage > totalPages) {
        setCurrentPage(totalPages);
      } else {
        setCurrentPage(nextPage);
      }
    } else {
      const prevPage = currentPage - 1;
      if (prevPage <= 1) {
        setCurrentPage(1);
      } else {
        setCurrentPage(prevPage);
      }
    }
  };

  const onItemClick = (item) => {
    if (activeSearchOption?.title === 'Affiliates') {
      setSelectedUser(item);
      setIsUserViewOpened(true);
    }
  };

  const navigateToProfile = (user, tabName) => {
    history.push(
      `/brokerid/${user?.userData?.username || ''}${
        tabName ? `/${tabName.toLowerCase()}` : ''
      }`,
      {
        selectedUser: user,
      },
    );
  };

  return (
    <div
      className={`landing-search-layout ${
        isSearchLayoutOpen ? 'd-flex' : 'd-none'
      }`}
    >
      <div className="search-layout-header">
        <div className="app-logo-container" onClick={onBack}>
          <img
            src={
              require('../../assets/images/affliate-app-dark-full-logo.svg')
                .default
            }
            alt=""
            className="app-img"
          />
        </div>
        <input
          type="text"
          className="search-input"
          placeholder={activeSearchOption.placeholder}
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          ref={inputRef}
        />
        {affilateList?.length <= 0 && (
          <div className="selected-option" onClick={toggleSearchType}>
            <img
              src={
                isSearchAsList
                  ? require('../../assets/images/search-as-list.svg').default
                  : require('../../assets/images/search-as-item.svg').default
              }
              alt=""
              className="option-img"
            />
          </div>
        )}
        <div className="selected-option" onClick={openOptionPopup}>
          <img src={activeSearchOption.icon} alt="" className="option-img" />
        </div>
        {activeSearchOption?.title === 'Affiliates' && (
          <div className="selected-option" onClick={openFilterPopup}>
            <img src={activeSearchFilter.icon} alt="" className="option-img" />
          </div>
        )}
      </div>
      {isUserViewOpened ? (
        <UserSelectedView
          selectedUser={selectedUser}
          setSelectedUser={setSelectedUser}
          affilateList={
            location.state?.isOpenUser
              ? [location.state?.userToOpen]
              : affilateList
          }
          nearbyItems={searchNearByItems}
        />
      ) : (
        <>
          <div className="search-list-container" ref={scrollRef}>
            {searchList ? (
              searchList.length > 0 ? (
                <List
                  height={listHeight}
                  itemCount={searchList.length}
                  itemSize={145}
                  width="100%"
                >
                  {({ index, style }) => {
                    const item = searchList[index];

                    return (
                      <div className="search-item-container" style={style}>
                        <div
                          className="search-item"
                          onClick={() => onItemClick(item)}
                        >
                          <div className="item-details">
                            <ProfileAvatar
                              avatar={item.profile_img}
                              name={item.name}
                              size={55}
                            />
                            <div className="name-container">
                              <div className="item-name">{item.name}</div>
                              <div className="item-email">{item.email}</div>
                            </div>
                          </div>
                          {activeSearchOption?.title === 'Affiliates' && (
                            <>
                              <div
                                className="item-feature-container center"
                                onClick={() =>
                                  navigateToProfile(item, 'Contacts')
                                }
                              >
                                <div className="item-feature">
                                  <div className="item-feature-value">
                                    {item.downlinesCount}
                                  </div>
                                  <div className="item-feature-label">
                                    Contacts
                                  </div>
                                </div>
                              </div>
                              <div
                                className="item-feature-container"
                                onClick={() =>
                                  navigateToProfile(item, 'Endorsements')
                                }
                              >
                                <div className="item-feature">
                                  <div className="item-feature-value">
                                    {item.followingCount}
                                  </div>
                                  <div className="item-feature-label">
                                    Endorsements
                                  </div>
                                </div>
                              </div>
                            </>
                          )}
                        </div>
                      </div>
                    );
                  }}
                </List>
              ) : (
                <div className="empty-message">
                  {!isSearchAsList && (
                    <div className="tooltip-container">
                      Switch To List Mode To See Similar{' '}
                      {activeSearchFilter.title}
                    </div>
                  )}
                  {`Sorry But We Cannot Find An ${
                    activeSearchOption?.title
                  } With The ${
                    activeSearchOption?.title === 'Affiliates'
                      ? activeSearchFilter.title
                      : 'Name'
                  } ${searchText}`}
                </div>
              )
            ) : (
              Array(10)
                .fill(1)
                .map((_, index) => (
                  <div key={index} className="search-item">
                    <div className="item-details">
                      <Skeleton circle width={55} height={55} />
                      <div className="name-container">
                        <Skeleton
                          width={150}
                          height={25}
                          className="item-name"
                        />
                        <Skeleton
                          width={200}
                          height={15}
                          className="item-email"
                        />
                      </div>
                    </div>
                    <div className="item-feature-container center">
                      <div className="item-feature">
                        <Skeleton
                          width={80}
                          height={35}
                          className="item-feature-value"
                        />
                        <Skeleton
                          width={90}
                          height={10}
                          className="item-feature-label"
                        />
                      </div>
                    </div>
                    <div className="item-feature-container">
                      <div className="item-feature">
                        <Skeleton
                          width={80}
                          height={35}
                          className="item-feature-value"
                        />
                        <Skeleton
                          width={90}
                          height={10}
                          className="item-feature-label"
                        />
                      </div>
                    </div>
                  </div>
                ))
            )}
          </div>
          {activeSearchOption?.title === 'Affiliates' && !searchText.trim() && (
            <div className="pagination-container">
              <div className="title">Results Per Page</div>
              {PAGE_COUNTS.map((item) => (
                <div
                  key={item.title}
                  className={`page-count-item ${
                    item.title === noOfItemPerPage.title ? 'active' : ''
                  }`}
                  onClick={() => setNoOfItemPerPage(item)}
                >
                  {item.title}
                </div>
              ))}
              <div
                className="page-control-button ml-auto"
                onClick={() => updatePage(false)}
              >
                <img
                  src={
                    require('../../assets/images/page-controll-arrow.svg')
                      .default
                  }
                  alt=""
                  className="page-control-icon"
                />
              </div>
              <div className="current-page">
                {totalPages ? (
                  `Page ${currentPage} Of ${totalPages}`
                ) : (
                  <Skeleton count={1} width={100} />
                )}
              </div>
              <div
                className="page-control-button"
                onClick={() => updatePage(true)}
              >
                <img
                  src={
                    require('../../assets/images/page-controll-arrow.svg')
                      .default
                  }
                  alt=""
                  className="page-control-icon right"
                />
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default LandingSearchLayout;

const PAGE_COUNTS = [
  { title: '50', count: 50 },
  { title: '100', count: 100 },
  // { title: 'All', count: 999999999 },
];
