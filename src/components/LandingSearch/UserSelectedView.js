import React from 'react';
import { useHistory } from 'react-router-dom';
import UserListCarousel from './UserListCarousel';

const UserSelectedView = ({
  selectedUser,
  setSelectedUser,
  affilateList,
  nearbyItems,
}) => {
  const history = useHistory();

  const STATS = [
    { title: 'Contacts', count: selectedUser?.downlinesCount || 0 },
    { title: 'Endorsed Brands', count: selectedUser?.followingCount || 0 },
    { title: 'Opportunities', count: 0 },
    { title: 'Introductions', count: 0 },
  ];

  const onItemClick = item => {
    history.push(
      `/brokerid/${selectedUser?.userData?.username || ''}${
        item ? `/${item.title.toLowerCase()}` : ''
      }`,
      {
        selectedUser,
      },
    );
  };

  return (
    <div className="user-selected-view-wrapper">
      <UserListCarousel
        affilateList={affilateList}
        selectedUser={selectedUser}
        setSelectedUser={setSelectedUser}
        nearbyItems={nearbyItems}
      />
      <div className="user-stats-container">
        {STATS.map((item, index) => (
          <div
            key={item.title}
            className="stats-item"
            onClick={() => onItemClick(item)}
          >
            <div className="stats-value">{item.count}</div>
            <div className="stats-label">{item.title}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserSelectedView;
