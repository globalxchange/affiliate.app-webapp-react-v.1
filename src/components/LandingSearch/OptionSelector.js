import React from 'react';
import PopupModalLayout from '../../layouts/PopupModalLayout';

const OptionSelector = ({
  isOpen,
  onClose,
  options,
  setActiveSearchOption,
  activeSearchOption,
}) => {
  return (
    <PopupModalLayout
      isOpen={isOpen}
      onClose={onClose}
      headerText="Select Category"
      width={800}
    >
      <div className="search-options-wrapper">
        {options.map(item => (
          <div
            key={item.title}
            className={`option-item ${
              activeSearchOption?.title === item.title ? 'active' : ''
            }`}
            onClick={() => {
              setActiveSearchOption(item);
              onClose();
            }}
          >
            <div className="bran-img-container">
              <img src={item.icon} alt="" className="option-image" />
            </div>
            <div className="option-name">{item.title}</div>
          </div>
        ))}
      </div>
    </PopupModalLayout>
  );
};

export default OptionSelector;
