/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import Slider from 'react-slick';
import ProfileAvatar from '../ProfileAvatar';

const UserListCarousel = ({
  selectedUser,
  setSelectedUser,
  affilateList,
  nearbyItems,
}) => {
  const carouselRef = useRef();
  const carouselWrapperRef = useRef();
  const [displayList, setDisplayList] = useState([]);
  const [itemHeight, setItemHeight] = useState(150);

  useLayoutEffect(() => {
    if (carouselWrapperRef.current) {
      const height = carouselWrapperRef.current.clientHeight;
      setItemHeight(height / 5 - 15);
    }
  }, [
    carouselWrapperRef.current,
    carouselWrapperRef.current ? carouselWrapperRef.current.clientHeight : 0,
  ]);

  useEffect(() => {
    if (affilateList) {
      const listLength = affilateList.length;
      const totalItems = 30;

      let list;

      if (listLength > totalItems) {
        let currentIndex = affilateList.findIndex(
          user => user.email === selectedUser.email,
        );

        currentIndex = currentIndex > 0 ? currentIndex : 0;
        let startPoint = currentIndex - totalItems / 2;
        startPoint = startPoint <= totalItems / 2 ? currentIndex : 0;
        let endPoint = currentIndex + totalItems;
        endPoint =
          startPoint > 0
            ? endPoint
            : endPoint + (totalItems / 2 - currentIndex);
        endPoint = endPoint <= listLength ? endPoint : listLength;

        // console.log('listLength', listLength);
        // console.log('currentIndex', currentIndex);
        // console.log('startPoint', startPoint);
        // console.log('endPoint', endPoint);

        list = affilateList.slice(startPoint, endPoint);

        const foundItem = list.find(item => item.email === selectedUser.email);
        // console.log('foundItem', foundItem);

        if (!foundItem) {
          list = [...list, affilateList[currentIndex]];
        }
      } else {
        const extraList = nearbyItems.slice(1, totalItems - listLength);

        list = [...affilateList, ...extraList];
      }

      // console.log('list', list);
      setDisplayList(list);
    }
  }, [affilateList, nearbyItems]);

  useEffect(() => {
    if (displayList && selectedUser) {
      const index = displayList.findIndex(
        user => user.email === selectedUser.email,
      );

      if (index > 0) {
        carouselRef.current.slickGoTo(index, true);
      }
    }
  }, [displayList]);

  const onSliderChange = newIndex => {
    if (displayList) {
      setSelectedUser(displayList[newIndex]);
    }
  };

  const containerScrollListener = e => {
    if (carouselRef.current) {
      if (e.deltaY < 0) {
        carouselRef.current.slickPrev();
      } else {
        carouselRef.current.slickNext();
      }
    }
  };

  return (
    <div
      ref={carouselWrapperRef}
      className="user-carousel-wrapper"
      onWheel={containerScrollListener}
    >
      <Slider
        ref={carouselRef}
        infinite
        slidesToShow={5}
        vertical
        verticalSwiping
        centerMode
        swipeToSlide
        className="carousel-container"
        draggable
        arrows={false}
        dots={false}
        focusOnSelect
        afterChange={onSliderChange}
        centerPadding={60}
      >
        {/* <div className="carousel-container"> */}
        {displayList?.map(item => (
          <div
            key={item._id || item.email}
            className="user-carousel-item-wrapper"
          >
            <div className="user-carousel-item-container">
              <div
                className={`user-carousel-item ${
                  item.email === selectedUser?.email ? 'active' : ''
                }`}
                style={{ height: itemHeight }}
              >
                <ProfileAvatar
                  avatar={item.profile_img}
                  name={item.name}
                  size={70}
                  className="user-img"
                />
                <div className="user-details">
                  <div className="user-name">{item.name}</div>
                  <div className="user-email">{item.email}</div>
                </div>
              </div>
            </div>
          </div>
        ))}
        {/* </div> */}
      </Slider>
    </div>
  );
};

export default UserListCarousel;
