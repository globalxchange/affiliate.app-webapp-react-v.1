import React, { useEffect ,useState, useContext, useRef } from 'react'
// import './Carer.scss'
 import "./Banner.scss"
 import backbutton from '../../assets/images/SideNavIcons/backbutton.svg';
 import { Drawer } from '@material-ui/core';
 import mainlogo from '../../assets/images/SideNavIcons/mainlogo.svg';
import a1 from '../../assets/images/SideNavIcons/a1.svg';
import a2 from '../../assets/images/SideNavIcons/a2.svg';
import a3 from '../../assets/images/SideNavIcons/a3.svg';
import a4 from '../../assets/images/SideNavIcons/a4.svg';
import a5 from '../../assets/images/SideNavIcons/a5.svg';
import a6 from '../../assets/images/SideNavIcons/a6.svg';
import a7 from '../../assets/images/SideNavIcons/a7.svg';
import b1 from '../../assets/images/SideNavIcons/b1.svg';
import b2 from '../../assets/images/SideNavIcons/b2.svg';
import b3 from '../../assets/images/SideNavIcons/b3.svg';
import b4 from '../../assets/images/SideNavIcons/b4.svg';
import b5 from '../../assets/images/SideNavIcons/b5.svg';
import b6 from '../../assets/images/SideNavIcons/b6.svg';
import b7 from '../../assets/images/SideNavIcons/b7.svg';
import b8 from '../../assets/images/SideNavIcons/b8.svg';

import NvestIcon from "../../assets/images/SideNavIcons/virtual.svg";
import DotsIcon from "../../assets/images/dotIcon.svg";
import MailIcon from "../../assets/images/mail-image.svg";
import IndIcon from "../../assets/images/usa-image.svg";
// import { MainContext } from './Context'
// import MainContextProvider from './Context'

import affilatemaiun from '../../assets/images/SideNavIcons/affilatemaiun.svg';
import NvestCard from "../../assets/images/usa-image.svg"

export default function Carer() {
//   const { icondisp, setopencard, setpagemask, settherightcard, setclosedisp, setcloserightdisp, seticondisp } = useContext(MainContext)
  const wrapperRef = useRef();
  const [expanded, setExpanded] = useState(false);
  const [expandedmail, setexpandedmail] = useState(false);
  useOutsideAlerter(wrapperRef);




  function useOutsideAlerter(ref) {
    useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */

      function handleClickOutside(event) {
        // console.log(ref.current, event.target, "kwjbfkwjbefc");
        if (ref.current && !ref.current.contains(event.target)) {
          // setopencard("translateY(-140%)")
          // setpagemask("")
          // setclosedisp("none")
          // settherightcard("translateY(-140%)")
          // setcloserightdisp("none")
        //   seticondisp("none")
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }


//   const openmenu = (e) => {
//     setpagemask("pagemask")
//     setopencard("translateX(0%)")
//     setclosedisp("")
//   }


//   const openrightcard = (e) => {
//     settherightcard("translateX(0%)")
//     setpagemask("pagemask")
//     setcloserightdisp("")
//   }

//   const openico = (e) => {
//     seticondisp("")
//   }




  return (
    <div className='carres'>
      <div className="nvest-card"  ref={wrapperRef}>
        {/* <img src={NvestCard} alt="nvest-icon" className='nvest-img' onClick={(e) =>
          window.open("http://globalxchange.com", "_blank")
        } /> */}
      </div>

       <img src={NvestIcon} alt="nvest-icon" className='left-icon'  onClick={(e) =>
       setExpanded(!expanded)
        }/> 
      <div className='vl-top'></div>
      <img src={DotsIcon} alt="dots-icon" className='left-icon'  />
      <div className='vl-top'></div>


      <div className='right-icons'>
        <div className='vl-top1'></div>
        <img src={MailIcon} alt="mail-icon" className='right-logo'
           onClick={() =>
            setexpandedmail(!expandedmail)
             }
        />
        <div className='vl-top1'></div>
        <img src={IndIcon} alt="ind-icon" className='right-logo' />
      </div>
      {/* <TopCard /> */}
      {/* <h5 style={{ fontWeight: "bold", marginRight: "35px" }}><img src={te} style={{ marginRight: '10px' }} />For Investors
      </h5>
      <h5 style={{ opacity: 0.5 }}><img src={re} style={{ marginRight: '10px' }} />For Entrepreneurs</h5> */}






      {/* <div className={pagemask}></div>

      <div className='sidemenu' style={{ transform: isMenuOpened }}>
        <div className='side-text'>
          <img src={CloseIcon} alt="close-icon" className='close-img' onClick={closemenu} style={{ display: closedisp }} />
          <p className="card-title">One Account. Mutiple Advantages</p>
          <p className='card-desc'>With An IndianOTC Account You Also Get Access To The INR Group Apps</p>
          {carddata.map(e => {
            return (
              <>
                <br />
                <div className='card-data'>
                  <img src={e.image} alt="indian-otc" className='company-logo' />
                  <div className='card-text'>
                    <p className='comp-title'>{e.title}</p>
                    <p className='comp-desc'>{e.desc}</p>
                  </div>
                </div>
              </>
            )
          })}
        </div>
      </div>
      <div className='rightmenu' style={{ transform: RightCard }}>
        <img src={CloseIcon} alt="close-icon" className='close-img-right' onClick={rightclose} style={{ display: theicon }} />
        <div className='side-text'>
          <p className="right-card-title">Connect With Us</p>
          <p className='right-card-desc'>We Would Love To Hear From You</p>
          {rightdata.map(e => {
            return (
              <>
                <br />
                <div className='card-data'>
                  <img src={e.image} alt="indian-otc" className='company-logo' />
                  <div className='card-text'>
                    <p className='comp-title'>{e.title}</p>
                    <p className='comp-desc'>{e.desc}</p>
                  </div>
                </div>
              </>
            )
          })}
        </div>
      </div> */}

<Drawer
        anchor="left"
        open={expanded}
        style={{ width: '450px' }}
        onClose={() =>
          setExpanded(!expanded)
           
           }
        //   onClose={() => setIsCopensationDrawerOpen(false)}
        // className="b-pr-mui"
      >
        <div className="main-side-left-content">
          <div className="top-img">
            <img src={mainlogo} />
            <img src={backbutton}
            onClick={() =>
              setExpanded(!expanded)
               }
            />
          </div>
          <div className="list-image">
            <div className="content-inside" onClick={()=>window.open("https://my.affiliate.app/","_blank")}>
              <div className="image-box">
                <img src={a1} />
              </div>
              <div>
                <h3>Affiliate App</h3>
                <p>Monetize Your Network</p>
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://engagement.app/","_blank")}>
              <div className="image-box">
                <img src={a2} />
              </div>
              <div>
                <h3>Engagement App</h3>
                <p>Turn Customers Into Affiliates</p>
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://linked.app/","_blank")}>
              <div className="image-box">
                <img src={a3} />
              </div>
              <div>
                <h3>Linked</h3>
                <p>Link Management Platform</p>
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://endorsement.app/","_blank")}>
              <div className="image-box">
                <img src={a4} />
              </div>
              <div>
                <h3>Endorsement</h3>
                <p>Endorse With Profit</p>
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://coaches.app/","_blank")}>
              <div className="image-box">
                <img src={a5} />
              </div>
              <div>
                <h3>Coaches</h3>
                <p>Grow Your Coaching Business</p>
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://influencecoin.com/","_blank")}>
              <div className="image-box">
                <img src={a6} />
              </div>
              <div>
                <h3>InfluenceCoin</h3>
                <p>Token For The Viral Group</p>
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://connection.app/","_blank")}>
              <div className="image-box">
                <img src={a7} />
              </div>
              <div>
                <h3>Connection</h3>
                <p>Monetize Your Audience</p>
              </div>
            </div>
          </div>

        </div>
      </Drawer>








      
<Drawer
        anchor="left"
        open={expandedmail}
        style={{ width: '450px' }}
        onClose={() =>
          setexpandedmail(!expandedmail)
           }
        //   onClose={() => setIsCopensationDrawerOpen(false)}
        // className="b-pr-mui"
      >
        <div className="main-side-left-content">
          <div className="top-img">
            <img src={affilatemaiun} />
            <img src={backbutton}
            onClick={() =>
              setexpandedmail(!expandedmail)
               }
            />
          </div>
          <div className="list-image">
            <div className="content-inside" onClick={()=>window.open("https://blog.affiliate.app/","_blank")}>
              <div className="image-box">
                <img src={b1} />
              </div>
              <div>
                <h3>Blog</h3>

              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("mailto:info@affiliate.app?subject&body","_blank")}>
              <div className="image-box">
                <img src={b2} />
              </div>
              <div>
                <h3>Mail</h3>
    
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://discord.gg/AeU3RcBU","_blank")}>
              <div className="image-box">
                <img src={b3} />
              </div>
              <div>
                <h3>Discord</h3>

              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://www.youtube.com/channel/UCX8sVE-v2L9DIOAFRsqVDmQ","_blank")}>
              <div className="image-box">
                <img src={b4} />
              </div>
              <div>
                <h3>Youtube</h3>
 
              </div>
            </div>
            <div className="content-inside" onClick={()=>window.open("https://www.instagram.com/affiliate.app/","_blank")}>
              <div className="image-box">
                <img src={b5} />
              </div>
              <div>
                <h3>Instagram</h3>

              </div>
            </div>
            <div className="content-inside" style={{opacity:"0.4"}}>
              <div className="image-box">
                <img src={b6} />
              </div>
              <div>
                <h3>WhatsApp</h3>
           
              </div>
            </div>
            <div className="content-inside"  style={{opacity:"0.4"}}>
              <div className="image-box">
                <img src={b7} />
              </div>
              <div>
                <h3>Telegram</h3>
   
              </div>
            </div>
            <div className="content-inside" style={{opacity:"0.4"}} >
              <div className="image-box">
                <img src={b8} />
              </div>
              <div>
                <h3>Connection</h3>
         
              </div>
            </div>
        
          </div>

        </div>
      </Drawer>
    </div>
  )
}
