import React from 'react';
import ProfileAvatar from './ProfileAvatar';

const LadingList = () => (
  <div className="lading-list-wrapper">
    <div className="list-container">
      <div className="list-header-container">
        <div className="list-header">Top Endorsers</div>
        <div className="list-sub-header">Across All Brands</div>
      </div>
      <div className="list">
        {Array(10)
          .fill(1)
          .map((_, index) => (
            <div key={index} className="list-item">
              <ProfileAvatar size={60} name="Shorupan P" />
              <div className="item-details">
                <div className="item-name">Shorupan P</div>
                <div className="item-email">shorupan@gmail.com</div>
              </div>
            </div>
          ))}
      </div>
    </div>
    <div className="list-container">
      <div className="list-header-container">
        <div className="list-header">Verified Affiliates</div>
        <div className="list-sub-header">Across All Brands</div>
      </div>
      <div className="list">
        {Array(10)
          .fill(1)
          .map((_, index) => (
            <div key={index} className="list-item">
              <ProfileAvatar size={60} name="Shorupan P" />
              <div className="item-details">
                <div className="item-name">Shorupan P</div>
                <div className="item-email">shorupan@gmail.com</div>
              </div>
            </div>
          ))}
      </div>
    </div>
  </div>
);

export default LadingList;
