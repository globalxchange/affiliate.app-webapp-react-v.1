import React from 'react';

const ComingSoon = () => {
  return (
    <div className="m-auto">
      <h1 className="m-auto mt-5" style={{ color: '#010b1e' }}>
        Coming Soon...
      </h1>
    </div>
  );
};

export default ComingSoon;
