const SelectedProductDetails = ({ selectedProduct, selectedEndorsement }) => (
  <>
    <img src={selectedProduct?.product_icon} alt="" className="product-img" />
    <div className="product-name">{selectedProduct?.product_name}</div>
    <div className="app-name">{selectedEndorsement?.app_code}</div>
  </>
);

export default SelectedProductDetails;
