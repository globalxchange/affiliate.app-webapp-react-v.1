/* eslint-disable no-restricted-globals */
import React, { useContext, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid/dist';
import jwt from 'jsonwebtoken';
import Axios from 'axios';
import Lottie from 'react-lottie';
import * as animationData from '../../../../../../assets/animation/loading.json';
import { VaultContext } from '../../../../../../contexts/VaultContext';
import { BrokerAppContext } from '../../../../../../contexts/BrokerAppContext';
import { formatterHelper } from '../../../../../../utils';
import { toast } from 'react-toastify';
import LocalStorageHelper from '../../../../../../utils/LocalStorageHelper';

const key = 'HUBQTVce7cUde4F';

const SelectAndConfirmAmount = ({
  coinObject,
  transCoin,
  isDeposit,
  setOpenModal,
  appFrom,
  defiTrustProfileId,
  coinSymbol,
  selectedCoin,
}) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData.default,
  };
  const { profileId, coinListObject, admin } = useContext(BrokerAppContext);

  // console.log('coinListObject', coinListObject);
  // console.log('coinSymbol', coinSymbol);

  let updateBalance = () => {};

  // console.log('selectedCoin', selectedCoin);

  const vaultCtx = useContext(VaultContext);
  if (vaultCtx) {
    updateBalance = vaultCtx.updateBalance;
    // selectedCoin = vaultCtx.selectedCoin;
  }
  const [depositAsset, setDepositAsset] = useState('');

  const [loading, setLoading] = useState(false);
  const [messageobj, setMessage] = useState('');
  const depositWithdraw = async () => {
    // const isValidTkn = await validateToken(email, token);

    const email = LocalStorageHelper.getAppEmail();
    const token = LocalStorageHelper.getAppToken();

    if (appFrom.app_code === 'gx') {
      const data = {
        email,
        token,
        amount: parseFloat(isDeposit ? depositAsset : selectedCoinAmount), // amount you need to be credited in SUbVAult:GXVAult
        from_coin: isDeposit ? transCoin : selectedCoin.coinSymbol, // coin from GXVAULT:SUBVAULT
        to_coin: isDeposit ? selectedCoin.coinSymbol : transCoin, // to COIN in SUBVAULT:GXVAULT
        identifier: uuidv4(), // unique Identifier
        app_code: defiTrustProfileId ? 'icetray' : 'ice',
        profile_id: defiTrustProfileId || profileId,
      };

      if (admin) {
        Axios.post(
          `https://comms.globalxchange.io/coin/vault/service/${
            isDeposit ? 'fund' : 'withdraw'
          }/gx`,
          data,
        )
          .then(res => {
            const { data } = res;
            setMessage(data);
            if (data.status) {
              toast.success('Transaction Succes');
              setOpenModal(false);
            } else {
              toast.error(data.message);
            }
          })
          .catch(err => {
            setMessage({
              status: false,
              message: err.message ? err.err : 'Something Went Wrong',
            });
            toast.error(err.message ? err.err : 'Something Went Wrong');
          })
          .finally(() => {
            setLoading(false);
            updateBalance();
          });
      }
    } else {
      const data = isDeposit
        ? {
            token,
            email,
            from: {
              app_code: appFrom.app_code,
              profile_id: appFrom.profile_id,
              coin: transCoin,
            },
            to: {
              app_code: defiTrustProfileId ? 'icetray' : 'ice',
              profile_id: defiTrustProfileId || profileId,
              coin: selectedCoin.coinSymbol,
            },
            to_amount: depositAsset, // the amount to be received in to COIN, here in the example, 100=>100 INR, as it is the to COIN
            identifier: uuidv4(), // unique txn identifier
            transfer_for: `Deposit To Ice From ${appFrom.app_name}`, // where or why this transfer is for
          }
        : {
            token,
            email,
            from: {
              app_code: defiTrustProfileId ? 'icetray' : 'ice',
              profile_id: defiTrustProfileId || profileId,
              coin: selectedCoin.coinSymbol,
            },
            to: {
              app_code: appFrom.app_code,
              profile_id: appFrom.profile_id,
              coin: transCoin,
            },
            to_amount: selectedCoinAmount, // the amount to be received in to COIN, here in the example, 100=>100 INR, as it is the to COIN
            identifier: uuidv4(), // unique txn identifier
            transfer_for: `Deposit To ${appFrom.app_name} From Ice`, // where or why this transfer is for
          };

      // console.log('Postdata', data);
      const encoded = jwt.sign(data, key, { algorithm: 'HS512' });
      Axios.post(
        'https://comms.globalxchange.io/coin/vault/service/transfer',
        { data: encoded },
      )
        .then(res => {
          const { data } = res;
          setMessage(data);
          if (data.status) {
            toast.success('Transaction Succes');
            setOpenModal(false);
          } else {
            toast.error(data.message);
          }
        })
        .catch(err => {
          setMessage({
            status: false,
            message: err.message ? err.err : 'Something Went Wrong',
          });
          toast.error(err.message ? err.err : 'Something Went Wrong');
        })
        .finally(() => {
          setLoading(false);
          updateBalance();
        });
    }
  };
  const [selectedCoinAmount, setSelectedCoinAmount] = useState('');
  const selectedChange = e => {
    if (!isNaN(e.target.value)) {
      setSelectedCoinAmount(e.target.value);
      if (coinListObject && coinListObject[selectedCoin.coinSymbol]) {
        setDepositAsset(
          e.target.value === ''
            ? ''
            : formatterHelper(
                (coinObject.price * e.target.value) /
                  coinListObject[selectedCoin.coinSymbol].price.USD,
                selectedCoin.coinSymbol,
              ).replace(',', ''),
        );
      }
    }
  };
  const depositOnChange = e => {
    if (!isNaN(e.target.value)) {
      setDepositAsset(e.target.value);
      setSelectedCoinAmount(
        e.target.value === ''
          ? ''
          : parseFloat(
              formatterHelper(
                (e.target.value *
                  coinListObject[selectedCoin.coinSymbol].price.USD) /
                  coinObject.price,
                transCoin,
              ).replace(',', ''),
            ),
      );
    }
  };
  useEffect(() => {
    if (messageobj !== '') {
      setTimeout(() => {
        setLoading(false);
      }, 2000);
    }

    // console.log('messageobj', messageobj);
  }, [messageobj]);

  const [readOnly, setReadOnly] = useState(false);

  return (
    <div className="select-vault coin">
      <div className="head">Confirm Deposit Details</div>
      {loading ? (
        <div className="content">
          <div className="m-auto">
            <Lottie options={defaultOptions} height={150} width={150} />
          </div>
        </div>
      ) : (
        <div className="content detail">
          <div className="label">
            Your {appFrom.app_name} {transCoin} Vault Will Be{' '}
            {!isDeposit ? 'Credited' : 'Debited'}
          </div>
          <label className="asset-item " tabIndex={0} role="button">
            <img
              src={coinListObject && coinListObject[transCoin].coinImage}
              className="icon my-auto"
              alt=""
            />
            <div className="name">
              {coinListObject && coinListObject[transCoin].coinName}
            </div>
            <div className="amount">
              <input
                value={selectedCoinAmount}
                onChange={selectedChange}
                type="text"
                placeholder={formatterHelper(0, transCoin)}
                readOnly={readOnly}
              />
              <small>{transCoin}</small>
            </div>
          </label>
          <div className="label">
            Your {defiTrustProfileId ? 'DefiTrust' : 'Assets.io'}{' '}
            {selectedCoin && selectedCoin.coinSymbol} Vault Will Be{' '}
            {isDeposit ? 'Credited' : 'Debited'}
          </div>
          <label className="asset-item " tabIndex={0} role="button">
            <img
              src={selectedCoin?.coinImage}
              className="icon my-auto"
              alt=""
            />
            <div className="name">{selectedCoin?.coinName}</div>
            <div className="amount">
              <input
                value={depositAsset}
                onChange={depositOnChange}
                type="text"
                placeholder={formatterHelper(
                  0,
                  selectedCoin && selectedCoin.coinSymbol,
                )}
                readOnly={readOnly}
              />
              <small>{selectedCoin && selectedCoin.coinSymbol}</small>
            </div>
          </label>
          <div className="buttons">
            <div className="deposit inv" onClick={() => setReadOnly(!readOnly)}>
              {readOnly ? 'Edit' : 'Done'}
            </div>
            <div
              className="deposit"
              onClick={() => {
                depositWithdraw();
                setLoading(true);
              }}
            >
              Confirm
            </div>
          </div>
        </div>
      )}
      <div className="footer" onClick={() => {}}>
        <span className="label">Transfer Fees </span>
        <span className="value">
          ${formatterHelper(0, 'USD')}
          <small>USD</small>
        </span>
      </div>
    </div>
  );
};

export default SelectAndConfirmAmount;
