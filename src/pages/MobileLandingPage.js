import React from 'react';
import { Link } from 'react-router-dom';

const MobileLandingPage = () => (
  <div className="mobile-landing-page-wrapper">
    <img
      src={require('../assets/images/affliate-app-dark-logo.svg').default}
      alt=""
      className="app-icon"
    />
    <img
      src={require('../assets/images/affliate-app-dark-full-logo.svg').default}
      alt=""
      className="broker-logo"
    />
    <div className="install-container">
      <Link className="install-action" to="app-installer/ios">
        <img
          src={require('../assets/images/donwload-from-app-store.svg').default}
          alt=""
          className="install-action-icon"
        />
      </Link>
      <Link className="install-action" to="app-installer/android">
        <img
          src={require('../assets/images/download-from-play-store.svg').default}
          alt=""
          className="install-action-icon"
        />
      </Link>
    </div>
    <div className="footer-action">
      <svg
        style={{ marginRight: 10 }}
        width="15"
        height="15"
        viewBox="0 0 15 15"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M14.1834 6.6621L1.20831 0.0934489C0.956916 -0.0335451 0.650379 -0.0306257 0.402229 0.0992876C0.152458 0.230661 0 0.470051 0 0.729878V13.8672C0 14.127 0.152458 14.3664 0.402229 14.4978C0.528736 14.5635 0.669842 14.597 0.810946 14.597C0.947185 14.597 1.08505 14.5664 1.20831 14.5036L14.1834 7.93496C14.4381 7.80504 14.597 7.56273 14.597 7.29853C14.597 7.03432 14.4381 6.79201 14.1834 6.6621Z"
          fill="white"
        />
      </svg>
      How It Works
    </div>
  </div>
);

export default MobileLandingPage;
