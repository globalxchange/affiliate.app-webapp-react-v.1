import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import SiteLayout from '../layouts/SiteLayout';
import BrandDetailsPage from './BrandDetailsPage';

const BrandsPage = () => {
  const [allBrands, setAllBrands] = useState();
  const [selectedBrand, setSelectedBrand] = useState();

  useEffect(() => {
    axios
      .get('https://teller2.apimachine.io/admin/allBankers')
      .then(({ data }) => {
        // console.log('data?.data', data?.data);

        setAllBrands(data?.data || '');
      });
  }, []);

  if (selectedBrand) {
    return (
      <BrandDetailsPage
        selectedBrand={selectedBrand}
        onBack={() => setSelectedBrand()}
      />
    );
  }

  return (
    <SiteLayout>
      <div className="brands-page-wrapper">
        <div className="row p-0 m-0 all-brands-container">
          {allBrands
            ? allBrands?.map(item => (
                <div
                  key={item._id}
                  className="col-sm-4 p-0 brand-item"
                  style={{ backgroundColor: `#${item.colorCode}` }}
                  onClick={() => setSelectedBrand(item)}
                >
                  <img
                    src={item.profilePicWhite}
                    alt=""
                    className="brand-icon"
                  />
                  <div className="brand-name">{item.displayName}</div>
                </div>
              ))
            : Array(6)
                .fill(1)
                .map((_, index) => (
                  <div key={index} className="col-sm-4 p-0 brand-item">
                    <Skeleton className="brand-item brand-skeleton" />
                  </div>
                ))}
        </div>
      </div>
    </SiteLayout>
  );
};

export default BrandsPage;
