import React from 'react';
import AppInstaller from '../components/AppInstaller';
import SiteLayout from '../layouts/SiteLayout';

const AppInstallerPage = () => {
  return (
    <SiteLayout>
      <div className="container flex-fill d-flex flex-column">
        <AppInstaller />
      </div>
    </SiteLayout>
  );
};

export default AppInstallerPage;
