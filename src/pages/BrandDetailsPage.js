import { Drawer } from '@material-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import SiteLayout from '../layouts/SiteLayout';

const BrandDetailsPage = ({ selectedBrand, onBack }) => {
  const [isCopensationDrawerOpen, setIsCopensationDrawerOpen] = useState(false);
  const [isLicenseDrawerIsOpen, setIsLicenseDrawerIsOpen] = useState(false);
  const [selectedLicense, setSelectedLicense] = useState();
  const [brandLicenses, setBrandLicenses] = useState();

  useEffect(() => {
    if (!isLicenseDrawerIsOpen) {
      setSelectedLicense();
    }
  }, [isLicenseDrawerIsOpen]);

  useEffect(() => {
    axios
      .get(`https://comms.globalxchange.io/gxb/product/get`, {
        params: { product_created_by: selectedBrand?.email },
      })
      .then(({ data }) => {
        // console.log('Data', data);
        setBrandLicenses(data?.products || '');
      })
      .catch((error) => {
        console.log('Error getting licenses', error);
      });
    return () => {};
  }, [selectedBrand]);

  const isSide = isCopensationDrawerOpen || isLicenseDrawerIsOpen;

  return (
    <SiteLayout>
      <div
        className="brand-details-wrapper"
        style={{ color: `#${selectedBrand?.colorCode}` }}
      >
        <img src={selectedBrand?.coverPicURL} alt="" className="cover-img" />
        <div
          className={`brand-details-container  ${
            isSide ? 'align-items-start' : ''
          }`}
        >
          <div
            className={`back-button ${isSide ? 'd-none' : ''}`}
            onClick={onBack}
          >
            <i className="fas fa-caret-left" />
            All Brands
          </div>
          <div className="brand-icon-container">
            <img
              src={selectedBrand?.profilePicURL}
              alt=""
              className="brand-icon"
            />
          </div>
          <div className="brand-name">
            <i
              className={`fas fa-caret-left ${isSide ? 'd-block' : 'd-none'}`}
            />
            {selectedBrand?.displayName}
          </div>
          <div className={`brand-description ${isSide ? 'text-left' : ''}`}>
            {selectedBrand?.description}
          </div>
          <div className="actions-container">
            <div
              className={`action-item ${
                isCopensationDrawerOpen ? 'active' : ''
              } `}
              onClick={() => setIsCopensationDrawerOpen(true)}
              style={
                isCopensationDrawerOpen
                  ? { backgroundColor: `#${selectedBrand?.colorCode}` }
                  : {}
              }
            >
              <img
                src={require('../assets/images/compensation-icon.svg').default}
                alt=""
                className="action-icon"
              />
              <div className="action-text">Compensation</div>
            </div>
            <div
              onClick={() => setIsLicenseDrawerIsOpen(true)}
              className={`action-item ${
                isLicenseDrawerIsOpen ? 'active' : ''
              } `}
              style={
                isLicenseDrawerIsOpen
                  ? { backgroundColor: `#${selectedBrand?.colorCode}` }
                  : {}
              }
            >
              <img
                src={require('../assets/images/credit-card-icon.svg').default}
                alt=""
                className="action-icon"
              />
              <div className="action-text">Licenses</div>
            </div>
          </div>
        </div>
      </div>
      <Drawer
        anchor="right"
        open={isCopensationDrawerOpen}
        onClose={() => setIsCopensationDrawerOpen(false)}
        className="b-pr-mui"
      >
        <div className="brand-prof-d">
          <h3 className="comp-head">Compensation</h3>
          <div className="lice">
            <div className="my-2 mr-3">
              <h6 style={{ color: '#9A9A9A' }}>
                What Is The Commission Structure?
              </h6>
              <div className="tranl">
                <div className="img-tx-tran">
                  <img
                    src={
                      require('../assets/images/compensation-icon.svg').default
                    }
                    alt="no_img"
                  />
                  <h6>Transactional</h6>
                </div>
                <p>
                  A cryptocurrency (or crypto currency) is a digital asset
                  designed to work as a medium of exchange wherein individual
                  coin.
                </p>
              </div>
            </div>
            <div className="rev-strem">
              <div className="my-2 mr-3">
                <h6 style={{ color: '#9A9A9A' }}>
                  What Are The Revenue Streams?
                </h6>
                <div className="tranl">
                  <div className="img-tx-tran">
                    <img
                      src={
                        require('../assets/images/compensation-icon.svg')
                          .default
                      }
                      alt="no_img"
                    />
                    <h6>Fiat To Crypto Trades</h6>
                  </div>
                  <p>
                    A cryptocurrency (or crypto currency) is a digital asset
                    designed to work as a medium of exchange wherein individual
                    coin.
                  </p>
                </div>
              </div>
              <div className="my-2 mr-3">
                <h6 style={{ color: '#9A9A9A' }}>
                  What Are The Revenue Streams?
                </h6>
                <div className="tranl">
                  <div className="img-tx-tran">
                    <img
                      src={
                        require('../assets/images/compensation-icon.svg')
                          .default
                      }
                      alt="no_img"
                    />
                    <h6>Fiat To Crypto Trades</h6>
                  </div>
                  <p>
                    A cryptocurrency (or crypto currency) is a digital asset
                    designed to work as a medium of exchange wherein individual
                    coin.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Drawer>
      <Drawer
        anchor="right"
        open={isLicenseDrawerIsOpen}
        onClose={() => setIsLicenseDrawerIsOpen(false)}
        className="b-pr-mui"
      >
        <div className="brand-prof-ins-cy">
          {selectedLicense ? (
            <div
              className="license-view-container"
              style={{ color: `#${selectedBrand?.colorCode}` }}
            >
              <div className="license-header">
                <div className="license-icon-container">
                  <img
                    src={selectedLicense?.product_icon}
                    alt=""
                    className="license-icon"
                  />
                </div>
                <div className="license-details">
                  <div className="license-name">
                    {selectedLicense.product_name}
                  </div>
                  <div className="license-intro">
                    {selectedLicense.sub_text}
                  </div>
                </div>
              </div>
              <div className="license-desc">
                {selectedLicense.full_description}
              </div>
              <div className="licenses-list">
                <div className="list-header">Licenses</div>
                <div className="subscription-item">
                  <div className="subscription-name">Monthly</div>
                  <div className="subscription-cost">
                    {selectedLicense?.billing_cycle?.monthly?.price}{' '}
                    {selectedLicense?.billing_cycle?.monthly?.coin}/Month
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <>
              <div className="d-flex">
                <div className="broker-li-head">
                  <h3>Broker Licenses</h3>
                  <h6>By {selectedBrand?.displayName}</h6>
                </div>
              </div>
              <div className="brok-all-lic">
                {brandLicenses?.map((item) => {
                  return (
                    <div key={item.product_id} className="brok-lices">
                      <div className="img-head-a">
                        <img src={item.product_icon} alt="" />
                        <h5 style={{ color: `#${selectedBrand?.colorCode}` }}>
                          {item.product_name}
                        </h5>
                      </div>
                      <p>{item.full_description}</p>
                      <div className="d-flex">
                        <div className="btn-brk-lice btn">See Expert</div>
                        <div
                          className="btn-brk-lice btn"
                          onClick={() => setSelectedLicense(item)}
                        >
                          Select
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </>
          )}
        </div>
      </Drawer>
    </SiteLayout>
  );
};

export default BrandDetailsPage;
