import React, { useEffect } from 'react'
import "./BrandsPage.scss"
import allbrands from "../../assets/images/allbrands.svg"
import capitalMarket from "../../assets/images/capitalMarket.svg"
import eCommerce from "../../assets/images/eCommerce.svg"
import Content from "../../assets/images/Content.svg"
import marketing from "../../assets/images/marketing.svg"
import Communication from "../../assets/images/Communication.svg"
import Axios from 'axios';
import Banner from "../../components/Banner"
import NavbarItem from "../../components/NavItem"
import Skeleton from 'react-loading-skeleton';
import toparrow from "../../assets/images/toparrow.svg"
export default function index() {
    const [collection, setcollection] = React.useState()
    const [info, setinfo] = React.useState("All Brands")
    const [loading, setloading] = React.useState(true)
    const [getbrandname, setgetbrandname] = React.useState([])
    useEffect(() => {
        allbrand()
    }, []);

    const allbrand = () => {
        setinfo("All Brands")
        setloading(true)
        Axios.get(`https://comms.globalxchange.io/gxb/apps/get`)
            .then((resp) => {
                const { data } = resp;
                setcollection(data.apps)
                console.log('Resp On Leads', data.apps);
                setloading(false)
            })
            .catch((error) => {

                console.log('Error On', error);
            });
    }
    useEffect(() => {

        Axios.get(` https://comms.globalxchange.io/gxb/apps/category/get`)
            .then((resp) => {
                const { data } = resp;
                let final=  data.categories.filter(item=>{
                    return   item.name !== "Accounting"
                })
                setgetbrandname(final)
                console.log('Remnmn msp On Leads', final);

            })
            .catch((error) => {

                console.log('Error On', error);
            });
    }, []);

    const infodata = (e) => {
        setinfo(e.name)
        setloading(true)
        Axios.get(`https://comms.globalxchange.io/gxb/apps/get?category_id=${e.category_id}`)
            .then((resp) => {
                const { data } = resp;
              
                setcollection(data.apps)
                //console.log('Resp On Leads', final);
                setloading(false)
            })
            .catch((error) => {

                console.log('Error On', error);
            });
    }


    const data = [
        {
            name: 'All Brands',
            img: allbrands,
        },
        // {
        //     name: "Capital Markets",
        //     img: capitalMarket
        // },
        // {
        //     name: "Ecommerce",
        //     img: eCommerce,
        // },
        // {
        //     name: "Content",
        //     img: Content,
        // },
        // {
        //     name: "Marketing",
        //     img: marketing,
        // },
        // {
        //     name: "Communications",
        //     img: Communication,

        // }
    ]
    return (
        <>
            <Banner />
            <NavbarItem />

            <div className='hmepageBrands'>
                <div className='left-side'>
                    {
                        data.map(item => {
                            return (
                                <div className='inside-section'
                                    onClick={allbrand}
                                    style={

                                        item?.name === info ?
                                            {
                                                backgroundColor: "rgba(228, 232, 235, 0.6)"
                                            }

                                            :
                                            {
                                                cursor: "pointer"

                                            }}
                                >
                                    {
                                        item?.name === info ?
                                            <img className='arrow' src={toparrow} />
                                            :
                                            ""
                                    }


                                    <img className='logo-brands' src={item.img}
                                        style={
                                            item.name == info ?
                                                {
                                                    width: "29px"
                                                }
                                                :
                                                {
                                                    width: "23px"

                                                }
                                        }

                                    />
                                    <h1 style={

                                        item?.name === info ?
                                            {
                                                fontWeight: "800",
                                                fontSize: "18px",

                                                color: "#212121",
                                            }

                                            :
                                            {
                                                opacity: "1"

                                            }}
                                    >{item.name}</h1>
                                </div>

                            )
                        })
                    }
                    <>
                        {
                            getbrandname.map(item => {
                                return (
                                    <div className='inside-section'
                                        onClick={() => infodata(item)}
                                        style={

                                            item?.name === info ?
                                                {
                                                    backgroundColor: "rgba(228, 232, 235, 0.6)",
                                                    paddingLeft: "8px"
                                                }

                                                :
                                                {

                                                    paddingLeft: "8px"

                                                }}
                                    >
                                        {
                                            item?.name === info ?
                                                <img className='arrow' src={toparrow} />
                                                :
                                                ""
                                        }


                                        <img src={item.icon}
                                            style={
                                                item.name == info ?
                                                    {
                                                        width: "35px",
                                                        height: "35px"
                                                    }
                                                    :
                                                    {
                                                        width: "35px",
                                                        height: "35px"

                                                    }
                                            }

                                        />
                                        <h1 style={

                                            item?.name === info ?
                                                {
                                                    fontWeight: "800",
                                                    fontSize: "18px",

                                                    color: "#212121",
                                                }

                                                :
                                                {
                                                    opacity: "1"

                                                }}
                                        >{item.name}</h1>
                                    </div>
                                )
                            })
                        }

                    </>

                </div>
                <div className="right-side-dash">
                    <div className='right-side'>
                        {
                            loading ?
                                <>

                                    {
                                        Array(20)
                                            .fill(1)
                                            .map((_, index) => {
                                                return (
                                                    <div className='cards'>
                                                        <div className='imgdiv-brands'>
                                                            <Skeleton
                                                                className="app-icon-container"
                                                                width={100}
                                                                height={100}
                                                            />

                                                        </div>

                                                        <div className='inline-image'>
                                                            <Skeleton
                                                                className="app-icon-container"
                                                                width={100}
                                                                height={30}
                                                            />

                                                            {/* <h1>{item?.app_name}</h1>
                                    <p>{item?.short_description}</p> */}
                                                            <div className='label-brands-section'>
                                                                <Skeleton

                                                                    width={150}
                                                                    height={20}
                                                                />

                                                            </div>
                                                        </div>
                                                    </div>

                                                )
                                            })
                                    }
                                </>

                                :
                                <>


                                    {
                                        collection?.map(item => {

                                            return (
                                                <div className='cards'>
                                                    <div className='imgdiv-brands'>
                                                        <img src={item?.app_icon} />
                                                    </div>

                                                    <div className='inline-image'>
                                                        <h1>{item?.app_name}</h1>
                                                        <p>{item?.short_description}</p>
                                                        <div className='label-brands-section'>
                                                            <label>Learn</label>
                                                            <label onClick={() => window.open(`https://${item.website}`, "_blank")}>Website</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </>
                        }
                    </div>
                </div>

            </div >
        </>
    )
}
