import React, { useState } from 'react';
import LandingLayout from '../../layouts/LandingLayout';
import RegisterSteps from '../../components/RegisterSteps';
import { EMPOWERED_APP_CODE } from '../../configs';

const RegistrationPage = () => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <LandingLayout>
      <div className="registration">
        <div className="d-flex flex-column mx-auto">
          <div className="page-wrapper flex-fill d-flex justify-content-center flex-column my-5">
            {isLoading || (
              <img
                className="broker-logo mx-auto"
                src={require('../../assets/images/empowered-icon.svg').default}
                alt="AffiliateApp™"
              />
            )}
            <div className="registration-form-view">
              <RegisterSteps
                isLoading={isLoading}
                setIsLoading={setIsLoading}
                appCode={EMPOWERED_APP_CODE}
              />
            </div>
          </div>
        </div>
      </div>
    </LandingLayout>
  );
};

export default RegistrationPage;
